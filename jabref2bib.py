#!/usr/bin/env python

import sys, os, re

if not os.path.isfile('journalList.txt'):
  import urllib
  urllib.urlretrieve("https://raw.githubusercontent.com/JabRef/jabref/master/src/main/resources/journals/journalList.txt", 
          filename="journalList.txt")
rulesfile = open('journalList.txt')

with open('abrv.bib', 'w') as abrvfile, open('full.bib', 'w') as fullfile:

  for rule in rulesfile.readlines():
    full, abbrv = rule.strip().split(" = ")

    acron = "".join(item[0].upper() for item in re.sub(r'[^\w\s]','',abbrv).split())

    society = re.match(r'^([A-Z][A-Z0-9_]*) ', abbrv)
    if society:
      acron = '%s_J_%s' % (society.group(1), acron[1:])
    else:
      acron = 'J_%s' % acron

    abrvfile.write('@STRING{%-15s= "%s"}\n' % (acron, re.sub(r'([A-Z0-9_]+)([ $])', r'{\1}\2', re.sub(r'"', r'', abbrv))))
    fullfile.write('@STRING{%-15s= "%s"}\n' % (acron, re.sub(r'([A-Z0-9_]+)([ $])', r'{\1}\2', re.sub(r'"', r'', full))))
    